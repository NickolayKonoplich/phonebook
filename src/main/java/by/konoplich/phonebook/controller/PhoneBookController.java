package by.konoplich.phonebook.controller;

import by.konoplich.phonebook.dto.*;
import by.konoplich.phonebook.service.PhoneBookService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/phonebook")
public class PhoneBookController {

    private final PhoneBookService service;

    public PhoneBookController(PhoneBookService service) {
        this.service = service;
    }

    /*
        http://localhost:8080/phonebook/person_list
     */
    @CrossOrigin(origins = "*") /* https://developer.mozilla.org/ru/docs/Web/HTTP/CORS */
    @GetMapping("/person_list")
    public List<PersonWithPhonesDto> listPerson() {
        return service.getAllPersons();
    }

    @CrossOrigin(origins = "*")
    @GetMapping("/person/{id}")
    public PersonDto getPerson(@PathVariable Long id) {
        return service.getPerson(id);
    }

    @CrossOrigin(origins = "*")
    @GetMapping("/phone_type_list")
    public List<PhoneTypeDto> getPhoneTypeList() {
        return service.getPhoneTypeList();
    }

    @CrossOrigin(origins = "*")
    @GetMapping("/person_details/{id}")
    public PersonDetailsDto getPersonDetails(@PathVariable("id") Long id) {
        return service.getPersonDetails(id);
    }

    @CrossOrigin(origins = "*")
    @DeleteMapping("/delete_persons")
    public void deleteUser(@RequestBody DeletePersonDto dto) {
        service.deletePersons(dto.getIds());
    }

    @CrossOrigin(origins = "*")
    @GetMapping("/subdivision_list")
    public List<SubdivisionDto> listSubdivision() {
        return service.getListSubdivision();
    }

    @CrossOrigin(origins = "*")
    @PostMapping("/filter_person_list")
    public List<PersonWithPhonesDto> filterPersons(@RequestBody FilterDto filter) {
        System.out.println("filterDto: " + filter);
        return service.filterPersons(filter.getName(), filter.getPosition(), filter.getSubdivisionId());
    }

    @CrossOrigin(origins = "*")
    @PostMapping("/add_person_phone")
    public SavePhoneDto addPersonPhone(@RequestBody SavePhoneDto phone) {
        System.out.println("phone: " + phone);
        return service.addPersonPhone(phone);
    }


    @CrossOrigin(origins = "*")
    @PostMapping("/add_person")
    public AddEditPersonDto addPerson(@RequestBody AddEditPersonDto person) {
        System.out.println("person: " + person);
        return service.addPerson(person);
    }

    @CrossOrigin(origins = "*")
    @GetMapping("/person_for_edit/{id}")
    public AddEditPersonDto getPersonForEdit(@PathVariable Long id) {
        return service.getPersonForEdit(id);
    }

    @CrossOrigin(origins = "*")
    @PutMapping("/update_person")
    public AddEditPersonDto updatePerson(@RequestBody AddEditPersonDto person) {
        System.out.println("person: " + person);
        return service.updatePerson(person);
    }

    @CrossOrigin(origins = "*")
    @DeleteMapping("/delete_phone/{id}")
    public void deletePhone(@PathVariable Long id) {
        service.deletePhone(id);
    }

}
