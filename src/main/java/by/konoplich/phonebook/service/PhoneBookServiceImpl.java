package by.konoplich.phonebook.service;

import by.konoplich.phonebook.dto.*;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Service
public class PhoneBookServiceImpl implements PhoneBookService {

    private final JdbcTemplate jdbcTemplate;
    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public PhoneBookServiceImpl(JdbcTemplate jdbcTemplate, NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }


    @Override
    @Transactional(readOnly = true)
    public List<PersonWithPhonesDto> getAllPersons() {
        return jdbcTemplate.query(
                "select id, name, position, subdivision, phones from phonebook.v_persons order by name",
                (rs, rowNum) ->
                        new PersonWithPhonesDto(
                                rs.getLong("id"),
                                rs.getString("name"),
                                rs.getString("position"),
                                rs.getString("subdivision"),
                                rs.getString("phones")
                        )
        );
    }

    @Override
    @Transactional(readOnly = true)
    public PersonDetailsDto getPersonDetails(Long personId) {
        PersonDetailsDto details = jdbcTemplate.queryForObject(
                "select id, name, position, subdivision from phonebook.v_persons where id=?",
                (rs, rowNum) ->
                        new PersonDetailsDto(
                                rs.getLong("id"),
                                rs.getString("name"),
                                rs.getString("position"),
                                rs.getString("subdivision")
                        ),
                personId
        );
        if (details != null) {
            List<PhoneDto> phones = jdbcTemplate.query(
                    "select id, phone_number, phone_type from phonebook.v_phones where person_id=? order by phone_type",
                    (rs, rowNum) ->

                            new PhoneDto(
                                    rs.getLong("id"),
                                    rs.getString("phone_number"),
                                    rs.getString("phone_type")
                            ),
                    personId
            );
            details.getPhones().addAll(phones);
        }

        return details;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void deletePersons(List<Long> ids) {
        System.out.println("ids: " + ids);
        if (!ids.isEmpty()) {
            String sqlQuery = "delete from phonebook.phone p where p.person_id in (:personIds)";
            Map<String, List<Long>> namedParameters = Collections.singletonMap("personIds", ids);
            int rows = namedParameterJdbcTemplate.update(sqlQuery, namedParameters);
            System.out.println("delete " + rows + " phones");
            sqlQuery = "delete from phonebook.person p where p.id in (:personIds)";
            rows = namedParameterJdbcTemplate.update(sqlQuery, namedParameters);
            System.out.println("delete " + rows + " persons");
        }
    }

    @Override
    public List<PersonWithPhonesDto> filterPersons(String name, String position, Long subdivisionId) {
        if (subdivisionId != -1) {
            return filterBySubdivision(name, position, subdivisionId);
        } else {
            return filterPersonWithoutSubdivision(name, position);
        }
    }

    private List<PersonWithPhonesDto> filterPersonWithoutSubdivision(String name, String position) {
        List<Object> params = new ArrayList<>();
        StringBuffer query = new StringBuffer("select id, name, position, subdivision, phones from phonebook.v_persons p where (1=1)");
        if (!ObjectUtils.isEmpty(name)) {
            query.append(" and upper(p.name) like upper(?)");
            params.add("%" + name + "%");
        }

        if (!ObjectUtils.isEmpty(position)) {
            query.append(" and  upper(p.position) like upper(?)");
            params.add("%" + position + "%");
        }
        query.append(" order by p.name");

        return jdbcTemplate.query(
                query.toString(),
                (rs, rowNum) ->
                        new PersonWithPhonesDto(
                                rs.getLong("id"),
                                rs.getString("name"),
                                rs.getString("position"),
                                rs.getString("subdivision"),
                                rs.getString("phones")
                        ),
                params.toArray()
        );
    }


    /*
            WITH RECURSIVE r AS (select id from phonebook.subdivisionId where id=1
            union
            select s.id from phonebook.subdivisionId s join r on s.parent_id = r.id)
            select p.id, p.name, p.position, s.title as subdivisionId, vpp.phones from phonebook.person p
            left join phonebook.subdivisionId s on s.id = p.subdivision_id
            left join phonebook.v_person_phones vpp on vpp.person_id = p.id
            where p.subdivision_id in (select id from r)
            and upper(p.name) like upper('%?%')
            and  upper(p.position) like upper('%?%')
         */
    private List<PersonWithPhonesDto> filterBySubdivision(String name, String position, Long subdivisionId) {
        List<Object> params = new ArrayList<>();
        StringBuilder query = new StringBuilder();
        query.append("WITH RECURSIVE r AS (select id from phonebook.subdivision where id=?\n" +
                "    union\n" +
                "    select s.id from phonebook.subdivision s join r on s.parent_id = r.id)\n" +
                "select p.id, p.name, p.position, s.title as subdivision, vpp.phones from phonebook.person p\n" +
                "left join phonebook.subdivision s on s.id = p.subdivision_id\n" +
                "left join phonebook.v_person_phones vpp on vpp.person_id = p.id\n" +
                "where p.subdivision_id in (select id from r)");
        params.add(subdivisionId);

        if (!ObjectUtils.isEmpty(name)) {
            query.append(" and upper(p.name) like upper(?)");
            params.add("%" + name + "%");
        }

        if (!ObjectUtils.isEmpty(position)) {
            query.append(" and  upper(p.position) like upper(?)");
            params.add("%" + position + "%");
        }
        query.append(" order by p.name");
        return jdbcTemplate.query(
                query.toString(),
                (rs, rowNum) ->
                        new PersonWithPhonesDto(
                                rs.getLong("id"),
                                rs.getString("name"),
                                rs.getString("position"),
                                rs.getString("subdivision"),
                                rs.getString("phones")
                        ),
                params.toArray()
        );
    }

    @Override
    public List<SubdivisionDto> getListSubdivision() {
        return jdbcTemplate.query(
                "select id, title from phonebook.subdivision order by id",
                (rs, rowNum) ->
                        new SubdivisionDto(
                                rs.getLong("id"),
                                rs.getString("title")
                        )
        );
    }

    @Override
    public void deletePhone(Long phoneId) {
        Object[] args = new Object[]{phoneId};
        jdbcTemplate.update("delete from phonebook.phone where id=?", args);
    }

    @Override
    public PersonDto getPerson(Long personId) {
        return jdbcTemplate.queryForObject(
                "select id, name, position, subdivision from phonebook.v_persons where id=?",
                (rs, rowNum) ->
                        new PersonDetailsDto(
                                rs.getLong("id"),
                                rs.getString("name"),
                                rs.getString("position"),
                                rs.getString("subdivision")
                        ),
                personId
        );
    }

    @Override
    public List<PhoneTypeDto> getPhoneTypeList() {
        return jdbcTemplate.query(
                "select id, name as title from phonebook.phone_type order by id",
                (rs, rowNum) ->
                        new PhoneTypeDto(
                                rs.getLong("id"),
                                rs.getString("title")
                        )
        );
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public SavePhoneDto addPersonPhone(SavePhoneDto phone) {
        String query = "INSERT INTO phonebook.phone (phone_number, phone_type, person_id) VALUES (?, ?, ?)";
        KeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection.prepareStatement(query, new String[]{"id"});
            ps.setString(1, phone.getNumber());
            ps.setLong(2, phone.getType());
            ps.setLong(3, phone.getPersonId());
            return ps;
        }, keyHolder);

        Long id = keyHolder.getKeyAs(Long.class);
        phone.setId(id);
        return phone;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public AddEditPersonDto addPerson(AddEditPersonDto person) {
        String query = "INSERT INTO phonebook.person (name, position, subdivision_id) VALUES (?, ?, ?)";
        KeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection.prepareStatement(query, new String[]{"id"});
            ps.setString(1, person.getName());
            ps.setString(2, person.getPosition());
            ps.setLong(3, person.getSubdivisionId());
            return ps;
        }, keyHolder);

        Long id = keyHolder.getKeyAs(Long.class);
        person.setId(id);
        return person;
    }

    @Override
    public AddEditPersonDto getPersonForEdit(Long personId) {
        return jdbcTemplate.queryForObject(
                "select id, name, position, subdivision_id from phonebook.person where id=?",
                (rs, rowNum) ->
                        new AddEditPersonDto(
                                rs.getLong("id"),
                                rs.getString("name"),
                                rs.getString("position"),
                                rs.getLong("subdivision_id")
                        ),
                personId
        );
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public AddEditPersonDto updatePerson(AddEditPersonDto person) {
        String query = "update phonebook.person set name=?, position=?, subdivision_id=? where id=?";
        this.jdbcTemplate.update(query, person.getName(), person.getPosition(), person.getSubdivisionId(), person.getId());
        return person;
    }
}
