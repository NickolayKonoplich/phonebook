package by.konoplich.phonebook.service;

import by.konoplich.phonebook.dto.*;

import java.util.List;

public interface PhoneBookService {

    List<PersonWithPhonesDto> getAllPersons();
    PersonDetailsDto getPersonDetails(Long personId);
    void deletePersons(List<Long> ids);

    List<PersonWithPhonesDto> filterPersons(String name, String position, Long subdivision);

    List<SubdivisionDto> getListSubdivision();

    void deletePhone(Long phoneId);

    PersonDto getPerson(Long personId);

    List<PhoneTypeDto> getPhoneTypeList();

    SavePhoneDto addPersonPhone(SavePhoneDto phone);

    AddEditPersonDto addPerson(AddEditPersonDto person);

    AddEditPersonDto getPersonForEdit(Long personId);

    AddEditPersonDto updatePerson(AddEditPersonDto person);


}
