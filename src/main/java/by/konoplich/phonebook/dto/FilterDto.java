package by.konoplich.phonebook.dto;

import java.io.Serializable;

public class FilterDto implements Serializable {
    private String name;
    private String position;
    private Long subdivisionId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Long getSubdivisionId() {
        return subdivisionId;
    }

    public void setSubdivisionId(Long subdivisionId) {
        this.subdivisionId = subdivisionId;
    }

    @Override
    public String toString() {
        return "FilterDto{" +
                "name='" + name + '\'' +
                ", position='" + position + '\'' +
                ", subdivisionId=" + subdivisionId +
                '}';
    }
}
