package by.konoplich.phonebook.dto;

import java.io.Serializable;

public class PhoneDto implements Serializable {

    private Long id;
    private String number;
    private String type;

    public PhoneDto() {
    }

    public PhoneDto(Long id, String number, String type) {
        this.id = id;
        this.number = number;
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
