package by.konoplich.phonebook.dto;

import java.util.ArrayList;
import java.util.List;

public class PersonDetailsDto extends PersonDto {

    private List<PhoneDto> phones = new ArrayList<>(0);

    public PersonDetailsDto(Long id, String name, String position, String subdivision) {
        super(id, name, position, subdivision);
    }

    public PersonDetailsDto(Long id, String name, String position, String subdivision, List<PhoneDto> phones) {
        super(id, name, position, subdivision);
        this.phones = phones;
    }

    public List<PhoneDto> getPhones() {
        return phones;
    }

    public void setPhones(List<PhoneDto> phones) {
        this.phones = phones;
    }
}
