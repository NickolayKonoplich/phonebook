package by.konoplich.phonebook.dto;

import java.io.Serializable;

public class AddEditPersonDto implements Serializable {

    private Long id;
    private String name;
    private String position;
    private Long subdivisionId;

    public AddEditPersonDto() {
    }

    public AddEditPersonDto(Long id, String name, String position, Long subdivisionId) {
        this.id = id;
        this.name = name;
        this.position = position;
        this.subdivisionId = subdivisionId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Long getSubdivisionId() {
        return subdivisionId;
    }

    public void setSubdivisionId(Long subdivisionId) {
        this.subdivisionId = subdivisionId;
    }

    @Override
    public String toString() {
        return "AddEditPersonDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", position='" + position + '\'' +
                ", subdivisionId=" + subdivisionId +
                '}';
    }
}
