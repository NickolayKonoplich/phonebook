package by.konoplich.phonebook.dto;

import java.io.Serializable;

public class SavePhoneDto implements Serializable {

    private Long id;
    private String number;
    private Long type;
    private Long personId;

    public SavePhoneDto() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }

    @Override
    public String toString() {
        return "SavePhoneDto{" +
                "id=" + id +
                ", number='" + number + '\'' +
                ", type=" + type +
                ", personId=" + personId +
                '}';
    }
}
