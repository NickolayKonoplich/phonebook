package by.konoplich.phonebook.dto;

import java.io.Serializable;

public class PersonDto implements Serializable {

    private Long id;
    private String name;
    private String position;
    private String subdivision;


    public PersonDto() {
    }

    public PersonDto(Long id, String name, String position, String subdivision) {
        this.id = id;
        this.name = name;
        this.position = position;
        this.subdivision = subdivision;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getSubdivision() {
        return subdivision;
    }

    public void setSubdivision(String subdivision) {
        this.subdivision = subdivision;
    }

}
