package by.konoplich.phonebook.dto;

public class PersonWithPhonesDto extends PersonDto {

    private String phones;


    public PersonWithPhonesDto(String phones) {
        this.phones = phones;
    }

    public PersonWithPhonesDto(Long id, String name, String position, String subdivision, String phones) {
        super(id, name, position, subdivision);
        this.phones = phones;
    }

    public String getPhones() {
        return phones;
    }

    public void setPhones(String phones) {
        this.phones = phones;
    }
}
