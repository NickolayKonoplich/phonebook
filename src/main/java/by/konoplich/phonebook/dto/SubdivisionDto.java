package by.konoplich.phonebook.dto;

import java.io.Serializable;

public class SubdivisionDto implements Serializable {
    private Long id;
    private String title;

    public SubdivisionDto() {
    }

    public SubdivisionDto(Long id, String title) {
        this.id = id;
        this.title = title;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
