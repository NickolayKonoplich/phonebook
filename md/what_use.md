# Что использовалось при разработке

## База данных

* PostgreSQL

## Backend

* Java 8
* String Boot 2.5.2
* Apache maven 3.6

## Frontend

* NodeJs v14.15.1
* Angular 12.1
