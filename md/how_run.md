# Как запустить

## База данных.

Установить БД PosgreSQL не ниже 9.6 с использованием recreate.bat из папки db или руками используя sql-скрипты из этой
же папки создать базу

## Backend

1. Установить Java 8 или выше
2. Установить maven 3.6 или выше
3. При необходимости отредактировать в application.properties параметры подключения к БД
4. С помощью maven собрать приложение
5. Запустить приложение

## Frontend

1. Устновить nodejs
2. Установить angular 12.1
3. Перейти в папку frontend
4. Установить все необходимые пакеты (npm install)
5. Запустить приложение фронтэнда (ng serve)
6. Открыть в браузере http://localhost:4200