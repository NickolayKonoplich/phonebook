# Редактирование персоны

Внешний вид формы редактирования персоны

!["Фильтр по ФИО"](img/edit_person.png "Фильтр по ФИО")

Форма аналогична [форме добавления персоны](add-person.md)

По нажатия на кнопку "Сохранить" так же производится проверка на то что поля заполнены.