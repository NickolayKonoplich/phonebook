@echo off
SET ERROR_LOG=error.log
echo >%ERROR_LOG%
SET PGCLIENTENCODING=utf-8

set SYS_USER=postgres
set /P SYS_PASSWORD=Please type postgres password(default:123):
IF "%SYS_PASSWORD%"=="" set SYS_PASSWORD=123
set /P DEFAULT_USER_PASSWORD=Please type default password for phonebook_admin(default:123):
IF "%DEFAULT_USER_PASSWORD%"=="" set DEFAULT_USER_PASSWORD=123

chcp 65001

set PGPASSWORD=%SYS_PASSWORD%
set USER_PASSWORD=%DEFAULT_USER_PASSWORD%
set USER_LOGIN=phonebook_admin
set USER_DB=phonebook_db
psql -h 127.0.0.1 -p 5432 -U %SYS_USER% -d postgres "--variable=USER_LOGIN=%USER_LOGIN%" "--variable=USER_DB=%USER_DB%" "--variable=USER_PWD=%USER_PASSWORD%" -f 00_drop_db.sql 2>>%ERROR_LOG%
psql -h 127.0.0.1 -p 5432 -U %SYS_USER% -d postgres "--variable=USER_LOGIN=%USER_LOGIN%" "--variable=USER_DB=%USER_DB%" "--variable=USER_PWD=%USER_PASSWORD%" -f 01_create_db_and_user.sql 2>>%ERROR_LOG%
psql -h 127.0.0.1 -p 5432 -U %SYS_USER% -d postgres -c "ALTER USER %USER_LOGIN% PASSWORD '%USER_PASSWORD%';" 2>>%ERROR_LOG%
set PGPASSWORD=%USER_PASSWORD%
psql -h 127.0.0.1 -p 5432 -U %USER_LOGIN% -d %USER_DB% -f 02_create_db_schema.sql 2>>%ERROR_LOG%
set PGPASSWORD=%SYS_PASSWORD%
set PGPASSWORD=%USER_PASSWORD%
psql -h 127.0.0.1 -p 5432 -U %USER_LOGIN% -d %USER_DB% -f 03_create_tables.sql 2>>%ERROR_LOG%
psql -h 127.0.0.1 -p 5432 -U %USER_LOGIN% -d %USER_DB% -f 04_insert_data.sql 2>>%ERROR_LOG%

set PGPASSWORD=%SYS_PASSWORD%

chcp 866