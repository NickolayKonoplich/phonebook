drop view if exists phonebook.v_person_phones;
drop view if exists phonebook.v_phones;
drop table if exists phonebook.phone;
drop table if exists phonebook.phone_type;
drop view if exists phonebook.v_persons;
drop table if exists phonebook.person;
drop table if exists phonebook.subdivision;


CREATE TABLE phonebook.subdivision
(
    id        bigserial primary key,
    parent_id bigint references phonebook.subdivision (id),
    title     varchar(250) not null
);
COMMENT ON TABLE phonebook.subdivision IS 'Таблица подразделений';
COMMENT ON COLUMN phonebook.subdivision.id IS 'Идентификатор';
COMMENT ON COLUMN phonebook.subdivision.parent_id IS 'Родительское подразделение';
COMMENT ON COLUMN phonebook.subdivision.title IS 'Наименование подразделения';


CREATE TABLE phonebook.person
(
    id             bigserial primary key,
    name           varchar(250) not null,
    position       varchar(300) not null,
    subdivision_id bigint       not null references phonebook.subdivision (id)
);
COMMENT ON TABLE phonebook.person IS 'Таблица сотрудников';
COMMENT ON COLUMN phonebook.person.id IS 'Идентификатор';
COMMENT ON COLUMN phonebook.person.name IS 'ФИО';
COMMENT ON COLUMN phonebook.person.position IS 'Должность';
COMMENT ON COLUMN phonebook.person.subdivision_id IS 'Подразделение. Ссылка на таблицу phonebook.subdivision';


CREATE TABLE phonebook.phone_type
(
    id   bigserial primary key,
    name varchar(250) not null
);
COMMENT ON TABLE phonebook.phone_type IS 'Таблица типов телефонов';
COMMENT ON COLUMN phonebook.phone_type.id IS 'Идентификатор';
COMMENT ON COLUMN phonebook.phone_type.name IS 'Наименование типа';

CREATE TABLE phonebook.phone
(
    id           bigserial primary key,
    phone_number varchar(30) not null,
    phone_type   bigint      not null references phonebook.phone_type (id),
    person_id    bigint      not null references phonebook.person (id)
);
COMMENT ON TABLE phonebook.phone IS 'Таблица типов телефонов';
COMMENT ON COLUMN phonebook.phone.id IS 'Идентификатор';
COMMENT ON COLUMN phonebook.phone.phone_number IS 'Номер телефона';
COMMENT ON COLUMN phonebook.phone.phone_type IS 'Тип телефона. Ссылка на таблицу типов телефона';
COMMENT ON COLUMN phonebook.phone.person_id IS 'Сотрудник, к которому относится телефон';



create or replace view phonebook.v_person_phones as
select p.person_id, string_agg(p.phone_number, ', ') as phones
from phonebook.phone p
group by p.person_id;

create or replace view phonebook.v_persons as
select p.id, p.name, p.position, s.title as subdivision, vpp.phones
from phonebook.person p
left join phonebook.subdivision s on s.id = p.subdivision_id
left join phonebook.v_person_phones vpp on vpp.person_id = p.id;

create or replace view phonebook.v_phones as
select p.id, p.person_id, p.phone_number, pt.name as phone_type
from phonebook.phone p
         left join phonebook.phone_type pt on pt.id = p.phone_type;