INSERT INTO phonebook.phone_type (id, name)
VALUES (1, 'Служебный');
INSERT INTO phonebook.phone_type (id, name)
VALUES (2, 'Личный номер');
INSERT INTO phonebook.phone_type (id, name)
VALUES (3, 'Служебный мобильный номер');

INSERT INTO phonebook.subdivision (id, title)
VALUES (1, 'Подразделение песен и плясок');
INSERT INTO phonebook.subdivision (id, parent_id, title)
VALUES (2, 1, 'Отдел песен');
INSERT INTO phonebook.subdivision (id, parent_id, title)
VALUES (3, 2, 'Сектор классической оперы');
INSERT INTO phonebook.subdivision (id, parent_id, title)
VALUES (4, 2, 'Сектор эстрадной песни');
INSERT INTO phonebook.subdivision (id, parent_id, title)
VALUES (5, 1, 'Отдел плясок');
INSERT INTO phonebook.subdivision (id, title)
VALUES (6, 'Подразделение тестирования');
INSERT INTO phonebook.subdivision (id, title)
VALUES (7, 'Подразделение магии');

INSERT INTO phonebook.person (id, name, position, subdivision_id)
VALUES (1, 'Михайлов Станислав Владимирович', 'Певец', 4);
INSERT INTO phonebook.person (id, name, position, subdivision_id)
VALUES (2, 'Петров Павел', 'Концертный директор', 2);

INSERT INTO phonebook.phone (id, phone_number, phone_type, person_id) VALUES (1, '+7(916)759-45-41', 2, 1);
INSERT INTO phonebook.phone (id, phone_number, phone_type, person_id) VALUES (2, '+7(916)760-00-00', 3, 1);
INSERT INTO phonebook.phone (id, phone_number, phone_type, person_id) VALUES (3, '+375(29)730-30-30', 3, 1);
INSERT INTO phonebook.phone (id, phone_number, phone_type, person_id) VALUES (4, '80657855555', 1, 2);
INSERT INTO phonebook.phone (id, phone_number, phone_type, person_id) VALUES (5, '123(24)53-56-58', 2, 2);
INSERT INTO phonebook.phone (id, phone_number, phone_type, person_id) VALUES (6, '+375(033)785-88-74', 3, 2);



ALTER SEQUENCE phonebook.phone_id_seq RESTART WITH 1000;
ALTER SEQUENCE phonebook.phone_type_id_seq RESTART WITH 1000;
ALTER SEQUENCE phonebook.person_id_seq RESTART WITH 1000;
ALTER SEQUENCE phonebook.subdivision_id_seq RESTART WITH 1000;