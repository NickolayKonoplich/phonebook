export interface Subdivision {
  id: number;
  title: string;
}