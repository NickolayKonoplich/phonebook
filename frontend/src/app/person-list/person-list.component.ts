import {Component, OnInit} from '@angular/core';
import {PersonListService} from '../person-list.service'
import {Person} from '../person';
import {Subdivision} from '../subdivision';
import {Filter} from '../filter';
import {Router} from '@angular/router'

@Component({
  selector: 'app-person-list',
  templateUrl: './person-list.component.html',
  styleUrls: ['./person-list.component.css'],
  providers: [PersonListService]
})
export class PersonListComponent implements OnInit {
  // https://stackoverflow.com/questions/15292278/how-do-i-remove-an-array-item-in-typescript

  persons: Person[] = [];
  subdivisions: Subdivision[] = [];
  selectedPersonsId: number[] = [];
  filter: Filter = {name: '', position: '', subdivisionId: -1};


  constructor(private personService: PersonListService, private router: Router) {
  }

  ngOnInit() {
    this.getPersons();
    this.getSubdivisions();
  }

  applyFilter(): void {
    // console.log('---------------');
    console.log('filer: ' + JSON.stringify(this.filter));
    if (this.filter.name == '' && this.filter.position == '' && this.filter.subdivisionId == -1) {
      console.log('Не указаны условия для использования фильтра');
      this.getPersons();
    } else {
      console.log('Применяем фильтр');
      this.personService.filterPersons(this.filter).subscribe({
        next: persons => {
          this.persons = persons
        },
        error: error => {
          // this.errorMessage = error.message;
          console.error('Ошибка применения фильтра', error);
        }
      });
    }
  }

  getPersons(): void {
    this.personService.getPersons().subscribe(persons => this.persons = persons);
  }

  getSubdivisions(): void {
    this.personService.getSubdivisions().subscribe(
      {
        next: subdivisions => {
          this.subdivisions = subdivisions
        },
        error: error => {
          // this.errorMessage = error.message;
          console.error('Ошибка получения списка подразделений', error);
        }
      });
  }

  checkUncheckPerson(id: number, values: any): void {
    console.log(id + ' - ' + values.currentTarget.checked);
    if (values.currentTarget.checked) {
      this.selectedPersonsId.push(id);
    } else {
      this.removeSelectedPersonId(id);

    }
    console.log('selectedPersonsId: ' + this.selectedPersonsId);
  }

  removeSelectedPersonId(personId: number): void {
    this.selectedPersonsId = this.selectedPersonsId.filter(item => item != personId);
  }

  deletePersons(): void {
    console.log('++++++++++++++++++');
    this.personService.deletePersons(this.selectedPersonsId).subscribe(s => {
      this.selectedPersonsId.forEach(id => {
          console.log(s);

          this.persons = this.persons.filter(person => person.id != id)
        }
      )
      this.selectedPersonsId = [];
      // console.log(s);
      // this.persons = this.persons.filter(person => person.id != id)
    });
  }

  addPerson(): void {
    this.router.navigate(['add-person']);

  }


}
