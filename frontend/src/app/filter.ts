export interface Filter {
  name: string;
  position: string;
  subdivisionId: number;
}