import {Component, OnInit} from '@angular/core';
import {AddEditPerson} from '../person';
import {Subdivision} from '../subdivision';
import {PersonListService} from '../person-list.service'
import {Location} from '@angular/common';
import {Router} from '@angular/router';

@Component({
  selector: 'app-add-person',
  templateUrl: './add-person.component.html',
  styleUrls: ['./add-person.component.css']
})
export class AddPersonComponent implements OnInit {

  person: AddEditPerson = {id: -1, name: '', position: '', subdivisionId: -1};
  subdivisions: Subdivision[] = [];
  isNameError: boolean = false;
  isPositionError: boolean = false;
  isSubdivisionError: boolean = false;

  constructor(private httpService: PersonListService, private location: Location, private router: Router) {
  }

  ngOnInit(): void {
    this.getSubdivisions();
  }

  savePerson(): void {
    console.log('savePerson');
    this.isNameError = !this.validName();
    this.isPositionError = !this.validPosition();
    this.isSubdivisionError = !this.validSudbivision();
    if (!this.isNameError && !this.isPositionError && !this.isSubdivisionError) {
      console.log('Сохраняем');
      this.httpService.addPerson(this.person).subscribe({
        next: person => {
          console.log('personId: ' + person.id);
          let url = `person/${person.id}/add-phone`;
          this.router.navigate([url]);
        },
        error: error => {
          console.error('Ошибка сохранения персоны', error);
        }
      });
    }
  }


  validName(): boolean {
    return this.person.name != '';
  }

  validPosition(): boolean {
    return this.person.position != '';
  }

  validSudbivision(): boolean {
    return this.person.subdivisionId != -1;
  }

  back(): void {
    this.location.back();
  }

  getSubdivisions(): void {
    this.httpService.getSubdivisions().subscribe(
      {
        next: subdivisions => {
          this.subdivisions = subdivisions
        },
        error: error => {
          console.error('Ошибка получения списка подразделений', error);
        }
      });
  }


}
