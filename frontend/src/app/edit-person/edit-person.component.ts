import {Component, OnInit} from '@angular/core';

import {AddEditPerson} from '../person';
import {Subdivision} from '../subdivision';
import {PersonListService} from '../person-list.service'
import {Location} from '@angular/common';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-edit-person',
  templateUrl: './edit-person.component.html',
  styleUrls: ['./edit-person.component.css']
})
export class EditPersonComponent implements OnInit {

  person: AddEditPerson = {id: -1, name: '', position: '', subdivisionId: -1};
  subdivisions: Subdivision[] = [];
  isNameError: boolean = false;
  isPositionError: boolean = false;
  isSubdivisionError: boolean = false;


  constructor(private httpService: PersonListService, private location: Location, private router: Router, private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.getSubdivisions();
    this.getPerson();
  }

  getPerson(): void {
    const routeParams = this.route.snapshot.paramMap;
    const personId = Number(routeParams.get('personId'));
    console.log('personId: ' + personId);
    this.httpService.getPersonForEdit(personId).subscribe(person => this.person = person);
  }

  savePerson(): void {
    console.log('savePerson');
    this.isNameError = !this.validName();
    this.isPositionError = !this.validPosition();
    this.isSubdivisionError = !this.validSudbivision();
    if (!this.isNameError && !this.isPositionError && !this.isSubdivisionError) {
      console.log('Сохраняем');
      this.httpService.updatePerson(this.person).subscribe({
        next: person => {
          console.log('personId: ' + person.id);
          let url = `person/${person.id}`;
          this.router.navigate([url]);
        },
        error: error => {
          console.error('Ошибка обновления персоны', error);
        }
      });
    }
  }


  validName(): boolean {
    return this.person.name != '';
  }

  validPosition(): boolean {
    return this.person.position != '';
  }

  validSudbivision(): boolean {
    return this.person.subdivisionId != -1;
  }

  back(): void {
    this.location.back();
  }

  getSubdivisions(): void {
    this.httpService.getSubdivisions().subscribe(
      {
        next: subdivisions => {
          this.subdivisions = subdivisions
        },
        error: error => {
          console.error('Ошибка получения списка подразделений', error);
        }
      });
  }


}
