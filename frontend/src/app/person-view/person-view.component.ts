import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';
import {PersonListService} from '../person-list.service'
import {PersonDateils} from '../person_details';

@Component({
  selector: 'app-person-view',
  templateUrl: './person-view.component.html',
  styleUrls: ['./person-view.component.css']
})
export class PersonViewComponent implements OnInit {

  person: PersonDateils | undefined;

  constructor(private personService: PersonListService, private route: ActivatedRoute, private location: Location, private router: Router) {
  }

  ngOnInit(): void {
    const routeParams = this.route.snapshot.paramMap;
    const personId = Number(routeParams.get('personId'));
    console.log('personId: ' + personId);
    this.personService.getPersonDateils(personId).subscribe(person => this.person = person);
  }

  deletePhone(phoneId: number): void {
    console.log('phoneId: ' + phoneId);
    if (this.person != undefined) {
      let phones = this.person.phones;
      this.person.phones = phones.filter(phone => phone.id != phoneId);
      this.personService.deletePhone(phoneId).subscribe();
    }
  }

  addPhone(): void {
    if (this.person != undefined) {
      let url = `person/${this.person.id}/add-phone`;
      this.router.navigate([url]);
    }
  }

  editPhone(): void {
    if (this.person != undefined) {
      let url = `edit-person/${this.person.id}`;
      this.router.navigate([url]);
    }
  }

  back(): void {
    this.location.back();
  }

}
