import {Injectable} from '@angular/core';

import {HttpClient, HttpHeaders} from '@angular/common/http';

import {Observable, of} from 'rxjs';
import {catchError, map, tap} from 'rxjs/operators';

import {Person, SimplePerson, PhoneType, SavePhoneDto, AddEditPerson} from './person';
import {PersonDateils} from './person_details';
import {Subdivision} from './subdivision';
import {Filter} from './filter';

@Injectable({
  providedIn: 'root'
})
export class PersonListService {

  private apiBaseUrl = 'http://localhost:8080/phonebook';


  constructor(private http: HttpClient) {
  }

  getPersons(): Observable<Person[]> {
    return this.http.get<Person[]>(this.apiBaseUrl + '/person_list')
      .pipe(
        tap(_ => this.log('fetched person list')),
        catchError(this.handleError<Person[]>('getPersons', []))
      );
  }


  getPersonDateils(id: number): Observable<PersonDateils> {
    const url = `${this.apiBaseUrl}/person_details/${id}`;
    return this.http.get<PersonDateils>(url).pipe(
      tap(_ => this.log(`fetched person id=${id}`)),
      catchError(this.handleError<PersonDateils>(`getPerson id=${id}`))
    );
  }

  getSimplePerson(id: number): Observable<SimplePerson> {
    const url = `${this.apiBaseUrl}/person/${id}`;
    return this.http.get<SimplePerson>(url).pipe(
      tap(_ => this.log(`fetched person id=${id}`)),
      catchError(this.handleError<SimplePerson>(`getSimplePerson id=${id}`))
    );
  }


  deletePersons(ids: number[]): Observable<any> {
    const url = `${this.apiBaseUrl}/delete_persons`;

    // return this.http.delete<Person>(url, this.httpOptions).pipe(
    //   tap(_ => this.log(`deleted persons ids=${ids}`)),
    //   catchError(this.handleError<Person>('deletePersons'))
    // );
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: {
        'ids': ids
      },
    };

    return this.http.delete(url, options);

  }

  filterPersons(filter: Filter): Observable<Person[]> {
    const url = `${this.apiBaseUrl}/filter_person_list`;
    let httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    };
    return this.http.post<Person[]>(url, filter, httpOptions);
  }

  getPersonForEdit(id: number): Observable<AddEditPerson> {
    const url = `${this.apiBaseUrl}/person_for_edit/${id}`;
    return this.http.get<AddEditPerson>(url);
  }


  addPersonPhone(phone: SavePhoneDto): Observable<SavePhoneDto> {
    const url = `${this.apiBaseUrl}/add_person_phone`;
    let httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    };
    return this.http.post<SavePhoneDto>(url, phone, httpOptions);
  }

  addPerson(phone: AddEditPerson): Observable<AddEditPerson> {
    const url = `${this.apiBaseUrl}/add_person`;
    let httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    };
    return this.http.post<AddEditPerson>(url, phone, httpOptions);
  }

  updatePerson(phone: AddEditPerson): Observable<AddEditPerson> {
    const url = `${this.apiBaseUrl}/update_person`;
    let httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    };
    return this.http.put<AddEditPerson>(url, phone, httpOptions);
  }


  getSubdivisions(): Observable<Subdivision[]> {
    return this.http.get<Subdivision[]>(this.apiBaseUrl + '/subdivision_list');
  }

  getPhoneTypes(): Observable<PhoneType[]> {
    return this.http.get<PhoneType[]>(this.apiBaseUrl + '/phone_type_list');
  }

  deletePhone(id: number): Observable<any> {
    const url = `${this.apiBaseUrl}/delete_phone/${id}`;
    let httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    };
    return this.http.delete<any>(url, httpOptions);
  }


  private log(message: string) {
    console.log(message)
  }


  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      this.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }


}
