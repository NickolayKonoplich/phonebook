import {Phone} from './phone';

export interface PersonDateils {
  id: number;
  name: string;
  position: string;
  subdivision: string;
  phones: Phone[];
}
