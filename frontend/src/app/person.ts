export interface Person {
  id: number;
  name: string;
  position: string;
  subdivision: string;
  phones: string;
}

export interface SimplePerson {
  id: number;
  name: string;
  position: string;
  subdivision: string;
}

export interface SavePhoneDto {
  id: number;
  number: string;
  type: number;
  personId: number;
}

export interface PhoneType {
  id: number;
  title: string;
}


export interface AddEditPerson {
  id: number;
  name: string;
  position: string;
  subdivisionId: number;
}