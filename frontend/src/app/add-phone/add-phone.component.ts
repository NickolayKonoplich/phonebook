import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {SimplePerson, SavePhoneDto, PhoneType} from '../person';
import {PersonListService} from '../person-list.service'
import {Location} from '@angular/common';

@Component({
  selector: 'app-add-phone',
  templateUrl: './add-phone.component.html',
  styleUrls: ['./add-phone.component.css']
})
export class AddPhoneComponent implements OnInit {

  phoneTypes: PhoneType[] = [];
  person: SimplePerson | undefined;
  phone: SavePhoneDto = {id: -1, number: '', type: -1, personId: -1};
  isNumberError: boolean = false;
  isTypeError: boolean = false;
  errorMessage: string = 'Текст ошибки';

  constructor(private httpService: PersonListService, private route: ActivatedRoute, private location: Location, private router: Router) {
  }

  ngOnInit(): void {
    this.getPersonInfo();
  }

  getPersonInfo(): void {
    const routeParams = this.route.snapshot.paramMap;
    const personId = Number(routeParams.get('personId'));
    console.log('personId: ' + personId);
    this.phone.personId = personId;
    this.httpService.getSimplePerson(personId).subscribe(person => this.person = person);
    this.httpService.getPhoneTypes().subscribe(pt => this.phoneTypes = pt);
  }


  savePhone(): void {
    this.isNumberError = !this.validNumber();
    this.isTypeError = !this.validType();
    if (!this.isNumberError && !this.isTypeError) {
      console.log('Сохраняем');
      this.httpService.addPersonPhone(this.phone).subscribe({
        next: phone => {
          console.log('phoneId: ' + phone.id);
          let url = `person/${phone.personId}`;
          this.router.navigate([url]);
        },
        error: error => {
          console.error('Ошибка сохранения телефона', error);
        }
      });
    }
  }

  validNumber(): boolean {
    return this.phone.number != '';
  }

  validType(): boolean {
    return this.phone.type != -1;
  }

  back(): void {
    this.location.back();
  }

}
