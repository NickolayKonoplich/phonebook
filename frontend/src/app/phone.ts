export interface Phone {
  id: number;
  number: string;
  type: string;
}