import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';

// import { AppRoutingModule } from './app-routing.module';
import {RouterModule} from '@angular/router';
import {AppComponent} from './app.component';
import {PersonListComponent} from './person-list/person-list.component';
import {PersonViewComponent} from './person-view/person-view.component';
import {FormsModule} from '@angular/forms';
import {AddPhoneComponent} from './add-phone/add-phone.component';
import {AddPersonComponent} from './add-person/add-person.component';
import {EditPersonComponent} from './edit-person/edit-person.component';

@NgModule({
  declarations: [
    AppComponent,
    PersonListComponent,
    PersonViewComponent,
    AddPhoneComponent,
    AddPersonComponent,
    EditPersonComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      {path: '', component: PersonListComponent},
      {path: 'person/:personId', component: PersonViewComponent},
      {path: 'person/:personId/add-phone', component: AddPhoneComponent},
      {path: 'add-person', component: AddPersonComponent},
      {path: 'edit-person/:personId', component: EditPersonComponent},
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
